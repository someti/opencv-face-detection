package com.mxm.test.opencv;

import org.opencv.core.*;
import org.opencv.core.Point;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;
import org.opencv.objdetect.CascadeClassifier;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;

import static org.opencv.imgcodecs.Imgcodecs.imencode;
import static org.opencv.imgcodecs.Imgcodecs.imwrite;
//人脸裁图
public class FaceCutting {
    //加载资源
  private static String relativelyPath = null;
    static{
        relativelyPath=System.getProperty("user.dir");
       //加载模型数据
       //System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
       System.load(relativelyPath+"\\src\\main\\resources\\dll\\opencv_java440.dll");
   }
    public static void main(String[] args) {
        try {
            xsimg(FaceDetector(relativelyPath+"\\src\\main\\resources\\3.jpg"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public static InputStream FaceDetector(String imgpath) throws IOException {
        CascadeClassifier faceDetector = new CascadeClassifier(relativelyPath+"\\src\\main\\resources\\haarcascade_xml\\haarcascade_frontalface_alt.xml");
        System.out.println("人脸检测开始……");
        if (faceDetector.empty()) {
            System.out.println("请引入xml模型文件……");
            return null;
        }
        // 创建图片tempFile
        File tempFile = new File(imgpath);
        // 读取创建的图片tempFile
        Mat image = Imgcodecs.imread(tempFile.toString());
        MatOfRect faceDetections = new MatOfRect();
        // 进行人脸检测
        faceDetector.detectMultiScale(image, faceDetections);
        System.out.println(String.format("检测到： %s", faceDetections.toArray().length));
        Integer index = 1;
        // 制图将图填充到image中
        for (Rect rect : faceDetections.toArray()) {
            Imgproc.rectangle(image, new Point(rect.x, rect.y), new Point(rect.x + rect.width, rect.y + rect.height),
                    new Scalar(0, 255, 0), 3);//画方框
            imageCut(tempFile.toString(), relativelyPath+"\\src\\main\\resources\\face\\"+index+".jpg", rect.x, rect.y, rect.width, rect.height);// 进行图片裁剪
            index++;
        }
        System.out.println("image"+image);
        return mat2InputStream(image);
    }
    public static void imageCut(String imagePath, String outFile, int posX, int posY, int width, int height) {
        // 原始图像
        Mat image = Imgcodecs.imread(imagePath);
        // 截取的区域：参数,坐标X,坐标Y,截图宽度,截图长度
        Rect rect = new Rect(posX, posY, width, height);
        // 两句效果一样
        Mat sub = image.submat(rect); // Mat sub = new Mat(image,rect);
        Mat mat = new Mat();
        Size size = new Size(width, height);
        Imgproc.resize(sub, mat, size);// 将人脸进行截图并保存
        imwrite(outFile, mat);
        System.out.println(String.format("图片裁切成功，裁切后图片文件为： %s", outFile));
    }

    public static byte[] toByteArray(File file) throws IOException {
        File f = file;
        if (!f.exists()) {
            throw new FileNotFoundException("file not exists");
        }
        ByteArrayOutputStream bos = new ByteArrayOutputStream((int) f.length());
        BufferedInputStream in = null;
        try {
            in = new BufferedInputStream(new FileInputStream(f));
            int buf_size = 1024;
            byte[] buffer = new byte[buf_size];
            int len = 0;
            while (-1 != (len = in.read(buffer, 0, buf_size))) {
                bos.write(buffer, 0, len);
            }
            return bos.toByteArray();
        } catch (IOException e) {
            e.printStackTrace();
            throw e;
        } finally {
            try {
                in.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            bos.close();
        }
    }
    //mat转输入流
    public static InputStream mat2InputStream(Mat mat)
    {
        MatOfByte mob = new MatOfByte();
        imencode(".jpg", mat, mob);
        byte[] byteArray = mob.toArray();
        return new ByteArrayInputStream(byteArray);
    }
    public static void xsimg(InputStream ewm) {
        JFrame f1=new JFrame("图片预览");
        f1.setBounds(300, 300, 420, 670);
        f1.setVisible(true);
        f1.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        BufferedImage bgg = null;
        try {
            bgg = ImageIO.read(ewm);
        } catch (IOException e) {
            e.printStackTrace();
        }
        ImageIcon img=new ImageIcon(bgg);
        JLabel bg=new JLabel(img);
        f1.getLayeredPane().add(bg, new Integer(Integer.MIN_VALUE));
        bg.setBounds(0, 0, img.getIconWidth(), img.getIconHeight());
        JPanel jp= (JPanel)f1.getContentPane();//强制类型转换
        jp.setOpaque(false);
        jp.setLayout(new FlowLayout());
        jp.add(bg);
    }
}
