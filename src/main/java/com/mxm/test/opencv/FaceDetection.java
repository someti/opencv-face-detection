package com.mxm.test.opencv;

import org.opencv.core.*;
import org.opencv.core.Point;
import org.opencv.imgproc.Imgproc;
import org.opencv.objdetect.CascadeClassifier;
import org.opencv.videoio.VideoCapture;
import org.opencv.videoio.Videoio;

import javax.swing.*;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;

/**
 * opencv人脸检测
 * 需要：opencv_java440.dll , haarcascade_frontalface_alt.xml(人脸数据)
 * 2020/9/11
 */
public class FaceDetection {

    public static void main(String[] args) {
        //加载资源
        String relativelyPath=System.getProperty("user.dir");
        System.load(relativelyPath+"\\src\\main\\resources\\dll\\opencv_java440.dll");
        //加载人脸数据
        CascadeClassifier cascadeClassifier = new CascadeClassifier(relativelyPath+"\\src\\main\\resources\\haarcascade_xml\\haarcascade_frontalface_alt.xml");
        //打开摄像头
        VideoCapture videoCapture = new VideoCapture(0);
        videoCapture.set(Videoio.CAP_PROP_FRAME_WIDTH,620);
        videoCapture.set(Videoio.CAP_PROP_FRAME_HEIGHT,440);
        //判断是否加载到摄像头
        if (!videoCapture.isOpened()){
            System.out.println("无法加载摄像头");
            return;
        }
        //获取视频分辨率
        int width = (int) videoCapture.get(3);
        int height = (int) videoCapture.get(4);
        //创建窗口，播放视频
        JFrame gui = new JFrame("人脸检测");
        //调整窗体的大小
        gui.setSize(width, height);
        //创建多维矩阵
        Mat frame = new Mat();
        JLabel jLabel = new JLabel();
        //循环处理视频数据
        while (true){
            //视频数据转换为opencv处理图片矩阵对象
            boolean have = videoCapture.read(frame);
            //进入人脸检测
            MatOfRect matOfRect = new MatOfRect();
            cascadeClassifier.detectMultiScale(frame,matOfRect);
            if(matOfRect.toArray().length != 0){
                System.out.println("检测到人脸... 数量: "+matOfRect.toArray().length);
            }
            for (Rect rect: matOfRect.toArray()) {
                Imgproc.rectangle(frame,new Point(rect.x, rect.y),new Point(rect.x+ rect.width, rect.y+ rect.height),new Scalar(0,255,0),1);
                System.out.println("人脸所处视频位置, x:"+rect.x +" y:"+rect.y);
            }
            if (!have) break;
            if (!frame.empty()){
                //将Mat对象转换成java图像容器可播放的图片对象,并刷新
                jLabel.setIcon(new ImageIcon(toBufferedImage(frame)));
                gui.add(jLabel);
                gui.repaint();
                jLabel.repaint();
                gui.setVisible(true);
            }
            try {
                Thread.sleep(0);
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }
    private static BufferedImage toBufferedImage(Mat matrix) {
        int type = BufferedImage.TYPE_BYTE_GRAY;
        if (matrix.channels() > 1) {
            type = BufferedImage.TYPE_3BYTE_BGR;
        }
        int bufferSize = matrix.channels() * matrix.cols() * matrix.rows();
        byte[] buffer = new byte[bufferSize];
        matrix.get(0, 0, buffer);
        // 获取所有的像素点
        BufferedImage image = new BufferedImage(matrix.cols(), matrix.rows(), type);
        final byte[] targetPixels = ((DataBufferByte) image.getRaster().getDataBuffer()).getData();
        System.arraycopy(buffer, 0, targetPixels, 0, buffer.length);
        return image;
    }

}
