package com.mxm.test.opencv;

import org.opencv.core.CvType;
import org.opencv.core.Mat;

public class tset {

    public static void main(String[] args) {
        try {
            //加载资源
            //1.  System.load(Core.NATIVE_LIBRARY_NAME);(没成功)
            //2.
            String relativelyPath=System.getProperty("user.dir");
            System.load(relativelyPath+"\\src\\main\\resources\\dll\\opencv_java440.dll");
            //创建多层矩阵对象
            Mat mat = Mat.eye(3,3, CvType.CV_8UC1);
            //输出矩阵
            System.out.println("Opencv导入成功: "+mat.dump());
        }catch (Exception e){
            e.printStackTrace();
            System.out.println("Opencv加载失败");
        }

    }

}
