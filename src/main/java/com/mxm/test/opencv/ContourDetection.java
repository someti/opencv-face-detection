package com.mxm.test.opencv;

import org.opencv.core.*;
import org.opencv.core.Point;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import static org.opencv.imgcodecs.Imgcodecs.imencode;

//轮廓是由一系列的点组成的集合，表现在图像中就是一条曲线。
//OpenCV3.2.0中提供了查找轮廓的方法：
//Imgproc.findContours(Mat image, List contours, Mat hierarchy, int mode, int method, Point offset)
//参数说明：
//image：8位单通道图像。
//contours：存储检测到的轮廓的集合。
//hierarchy：可选的输出向量，包含了图像轮廓的拓扑信息。
//mode：轮廓检索模式。有如下几种模式：
//1、RETR_EXTERNAL只检测最外围的轮廓
//2、RETR_LIST提取所有的轮廓,不建立上下等级关系,只有兄弟等级关系
//3、RETR_CCOMP提取所有轮廓,建立为双层结构
//4、RETR_TREE提取所有轮廓,建立网状结构
//method：轮廓的近似方法。取值如下：
//1、CHAIN_APPROX_NONE获取轮廓的每一个像素,像素的最大间距不超过1
//2、CHAIN_APPROX_SIMPLE压缩水平垂直对角线的元素,只保留该方向的终点坐标(也就是说一条中垂线a-b,中间的点被忽略了)
//3、CHAIN_APPROX_TC89_LI使用TEH_CHAIN逼近算法中的LI算法
//4、CHAIN_APPROX_TC89_KCOS使用TEH_CHAIN逼近算法中的KCOS算法
//offset：每个轮廓点的可选偏移量。
public class ContourDetection {
    //加载资源
    private static String relativelyPath="";
    static {
        relativelyPath=System.getProperty("user.dir");
        //System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
        System.load(relativelyPath+"\\src\\main\\resources\\dll\\opencv_java440.dll");
    }
    public static void main(String[] args) {
            Mat src = Imgcodecs.imread(relativelyPath+"\\src\\main\\resources\\3.jpg");
            Mat dst = src.clone();
            Imgproc.cvtColor(dst, dst, Imgproc.COLOR_BGRA2GRAY);
            Imgproc.adaptiveThreshold(dst, dst, 255, Imgproc.ADAPTIVE_THRESH_MEAN_C,
                    Imgproc.THRESH_BINARY_INV, 3, 3);
            List<MatOfPoint> contours = new ArrayList<MatOfPoint>();
            Mat hierarchy = new Mat();
            Imgproc.findContours(dst, contours, hierarchy, Imgproc.RETR_EXTERNAL, Imgproc.CHAIN_APPROX_SIMPLE,
                    new Point(0, 0));
            System.out.println(contours.size());
            for (int i = 0; i < contours.size(); i++)
            {
                Imgproc.drawContours(src, contours, i, new Scalar(0, 0, 0, 0), 1);
            }
//            Imgcodecs.imwrite(relativelyPath+"\\src\\main\\resources\\002.jpg", src);
            xsimg(mat2InputStream(src));
    }
    //mat转输入流
    public static InputStream mat2InputStream(Mat mat)
    {
        MatOfByte mob = new MatOfByte();
        imencode(".jpg", mat, mob);
        byte[] byteArray = mob.toArray();
        return new ByteArrayInputStream(byteArray);
    }
    public static void xsimg(InputStream ewm) {
        JFrame f1=new JFrame("预览图片");
        f1.setBounds(300, 300, 420, 670);
        f1.setVisible(true);
        f1.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        BufferedImage bgg = null;
        try {
            bgg = ImageIO.read(ewm);
        } catch (IOException e) {
            e.printStackTrace();
        }
        ImageIcon img=new ImageIcon(bgg);
        JLabel bg=new JLabel(img);
        f1.getLayeredPane().add(bg, new Integer(Integer.MIN_VALUE));
        bg.setBounds(0, 0, img.getIconWidth(), img.getIconHeight());
        JPanel jp= (JPanel)f1.getContentPane();//强制类型转换
        jp.setOpaque(false);
        jp.setLayout(new FlowLayout());
        jp.add(bg);
    }
}
