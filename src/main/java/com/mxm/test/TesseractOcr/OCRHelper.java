package com.mxm.test.TesseractOcr;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Tesseract-OCR图文识别
 */
public class OCRHelper {
    private static final String LANG_OPTION = "-l";
    //获取系统换行符
    private static final String EOL = System.getProperty("line.separator");
    //获取项目路径
    private static String Path = System.getProperty("user.dir");
    //tesseract.exe路径
    private static String tessPath = Path+"/src/main/resources/tesseract-ocr/tesseract.exe";
    public static void main(String[] args) {
        try {
            //图片文件：此图片是需要被识别的图片
            File file = new File(Path+"/src/main/resources/003.png");
            String recognizeText = recognizeText(file);
            System.out.print("==============================================");
            System.out.print(recognizeText);
            System.out.print("==============================================");
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    /**
     * @param imageFile   传入的图像文件
     * @return 识别后的字符串
     */
    public static String recognizeText(File imageFile) throws Exception {
        /**
         * 设置输出文件的保存的文件目录
         */
        File outputFile = new File(imageFile.getParentFile(), "output");
        StringBuffer strB = new StringBuffer();
        List<String> cmd = new ArrayList<String>();
        System.out.println("===========os.name:"+System.getProperties().getProperty("os.name"));
        cmd.add(tessPath);
        cmd.add("");
        cmd.add(outputFile.getName());
        cmd.add(LANG_OPTION);
        cmd.add("chi_sim");
        //cmd.add("eng");
        ProcessBuilder pb = new ProcessBuilder();
        /**
         *设置此流程构建器的工作目录。
         */
        pb.directory(imageFile.getParentFile());
        cmd.set(1, imageFile.getName());
        pb.command(cmd);
        pb.redirectErrorStream(true);
        long startTime = System.currentTimeMillis();
        System.out.println("开始时间：" + startTime);
        Process process = pb.start();
        // tesseract.exe 1.jpg 1 -l chi_sim
        //不习惯使用ProcessBuilder的，也可以使用Runtime，效果一致
        // Runtime.getRuntime().exec("tesseract.exe 1.jpg 1 -l chi_sim");
        /**
         * 流程的退出值。按照惯例，0表示正常
         * 终止.
         */
//        System.out.println(cmd.toString());
        int w = process.waitFor();
        if (w == 0){// 0代表正常退出
            BufferedReader in = new BufferedReader(new InputStreamReader(
                    new FileInputStream(outputFile.getAbsolutePath() + ".txt"),"UTF-8"));
            String str;
            while ((str = in.readLine()) != null) {
                if(!"".equals(str)) {
                    strB.append(str.replaceAll("\\s*", "")).append(EOL);
                }
            }
            in.close();
            long endTime = System.currentTimeMillis();
            System.out.println("结束时间：" + endTime);
            System.out.println("耗时：" + (endTime - startTime) + "毫秒");
        } else {
            String msg;
            switch (w) {
                case 1:
                    msg = "访问文件时出错。图片的文件名中可能有空格。";
                    break;
                case 29:
                    msg = "无法识别图像或其所选区域。";
                    break;
                case 31:
                    msg = "不支援的图片格式。";
                    break;
                default:
                    msg = "发生错误。";
            }
            throw new RuntimeException(msg);
        }
        new File(outputFile.getAbsolutePath() + ".txt").delete();
        return strB.toString();
    }
}